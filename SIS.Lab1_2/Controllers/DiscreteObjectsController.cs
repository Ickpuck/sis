﻿using SIS.Lab1_2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIS.Lab1_2.Views
{
    public class DiscreteObjectsController : Controller
    {
        private SisLab1DbContext db;

        public DiscreteObjectsController()
        {
            db = new SisLab1DbContext();
        }

        private User AppUser
        {
            get
            {
                return Session["UserData"] as Models.User;
            }
        }

        // GET: DiscreteObjects
        public ActionResult Index()
        {
            if (AppUser.IsInRole("Admin"))
            {
                List<User> users = db.Users.ToList();
                ViewBag.UsersForObjects = users;
                return View(db.DiscreteObjects.ToList());
            }
            else
            {
                return View("IndexForUsers", db.DiscreteObjects.Where(u => u.UserId == AppUser.Id));
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (AppUser.IsInRole("admin"))
            {
                SelectList users = new SelectList(db.Users, "Id", "Name");
                ViewBag.Users = users;
                return View();
            }
            else
            {
                return View("CreateForUsers");
            }
        }
        [HttpPost]
        public ActionResult Create(DiscreteObject obj)
        {
            if (AppUser.IsInRole("admin"))
            {
                DiscreteObject res = new DiscreteObject();
                res = obj;
                res.Own = true;
                db.DiscreteObjects.Add(res);
                db.SaveChanges();

                DiscreteObject addedEntity = db.DiscreteObjects.FirstOrDefault(o => o.Own && o.ObjectId == 0);
                var original = db.DiscreteObjects.Find(addedEntity.Id);
                addedEntity.ObjectId = addedEntity.Id;
                db.Entry(original).CurrentValues.SetValues(addedEntity);
                db.SaveChanges();

                return RedirectToAction("Index", "DiscreteObjects");
            }
            else
            {
                DiscreteObject res = new DiscreteObject();
                res = obj;
                res.Own = true;
                res.Read = true;
                res.Write = true;
                res.TG = true;
                res.UserId = AppUser.Id;
                db.DiscreteObjects.Add(res);
                db.SaveChanges();

                DiscreteObject addedEntity = db.DiscreteObjects.FirstOrDefault(o => o.Own && o.ObjectId == 0);
                var original = db.DiscreteObjects.Find(addedEntity.Id);
                addedEntity.ObjectId = addedEntity.Id;
                db.Entry(original).CurrentValues.SetValues(addedEntity);
                db.SaveChanges();

                return RedirectToAction("Index", "DiscreteObjects");
            }
        }


        [HttpGet]
        public ActionResult GrantRights()
        {
            SelectList users = new SelectList(db.Users, "Id", "Name");
            ViewBag.Users = users;
            SelectList objects = new SelectList(db.DiscreteObjects.Where(o => o.UserId == AppUser.Id), "Name", "Name");
            ViewBag.Objects = objects;
            return View();
        }
        [HttpPost]
        public ActionResult GrantRights(DiscreteObject obj)
        {
            SelectList users = new SelectList(db.Users, "Id", "Name");
            ViewBag.Users = users;
            SelectList objects = new SelectList(db.DiscreteObjects.Where(o => o.UserId == AppUser.Id), "Name", "Name");
            ViewBag.Objects = objects;
            DiscreteObject objFromDb = db.DiscreteObjects.FirstOrDefault(o => o.UserId == AppUser.Id && obj.Name == o.Name);

            if (db.DiscreteObjects.FirstOrDefault(o => o.UserId == obj.UserId && obj.Name == o.Name) != null)
            {
                ModelState.AddModelError("Name", "User already has rights for this object");
                return View(obj);
            }

            if (!objFromDb.TG && (obj.Read || obj.Write))
            {
                ModelState.AddModelError("Name", "No TG rights");
                return View(obj);
            }
            if (!objFromDb.Own && (obj.Own || obj.TG))
            {
                ModelState.AddModelError("Name", "You are not owner of an object");
                return View(obj);
            }
            DiscreteObject objForGrant = new DiscreteObject();
            objForGrant.Name = obj.Name;
            objForGrant.UserId = obj.UserId;
            objForGrant.ObjectId = objFromDb.ObjectId;
            objForGrant.Read = obj.Read;
            objForGrant.Write = obj.Write;            
            objForGrant.TG = obj.TG;
            objForGrant.Own = obj.Own;

            if (obj.Own)
            {
                var original = db.DiscreteObjects.Find(objFromDb.Id);
                objFromDb.Own = false;
                db.Entry(original).CurrentValues.SetValues(objFromDb);
            }

            db.DiscreteObjects.Add(objForGrant);
            db.SaveChanges();
            return View();
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            SelectList users = new SelectList(db.Users, "Id", "Name");
            ViewBag.Users = users;
            SelectList objects = new SelectList(db.DiscreteObjects, "Name", "Name");
            ViewBag.Objects = objects;
            DiscreteObject objToEdit = db.DiscreteObjects.Find(id);
            return View(objToEdit);
        }
        [HttpPost]
        public ActionResult Edit(DiscreteObject obj)
        {
            DiscreteObject objFromDb = db.DiscreteObjects.FirstOrDefault(o => o.Own && obj.Name == o.Name);
            obj.ObjectId = objFromDb.ObjectId;
            db.DiscreteObjects.Attach(obj);
            db.Entry(obj).State = System.Data.EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            DiscreteObject objToDelete = db.DiscreteObjects.Find(id);
            return View(objToDelete);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            DiscreteObject objToDelete = db.DiscreteObjects.Find(id);
            if (!objToDelete.Own && !AppUser.IsInRole("admin"))
            {
                ModelState.AddModelError("Name", "You are not owner of an object");
                return View(objToDelete);
            }
            if (AppUser.IsInRole("admin"))
            {
                if (objToDelete.Own)
                {
                    foreach (DiscreteObject item in db.DiscreteObjects.Where(o => o.ObjectId == objToDelete.ObjectId))
                    {
                        db.DiscreteObjects.Remove(item);
                    } 
                }
                else
                {
                    db.DiscreteObjects.Remove(objToDelete);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            foreach (DiscreteObject item in db.DiscreteObjects.Where(o => o.ObjectId == objToDelete.ObjectId))
            {
                db.DiscreteObjects.Remove(item);
            }            
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}