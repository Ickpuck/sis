﻿using SIS.Lab1_2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using Microsoft.Owin.Security;

namespace SIS.Lab1_2.Controllers
{
    public class AccountController : Controller
    {
        private SisLab1DbContext db;

        public AccountController()
        {
            db = new SisLab1DbContext();
        }

        // GET: Account
        public ActionResult Index()
        {

            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            User user = new User();
            user = db.Users.FirstOrDefault(a => a.Name == model.Name);
            if (user != null)
            {
                SaltedPassword pw = new SaltedPassword();
                pw.PasswordHash = user.Password;
                pw.Salt = user.Salt;
                bool verificationResult = PasswordHasher.VerifyPassword(model.Password, pw);
                if (!verificationResult)
                {
                    ModelState.AddModelError("Password", "Wrong password");
                    return View(model);
                }
                Session["UserData"] = user;
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("Name", "User doesn't exist");
            return View(model);
        }

        public ActionResult LogOff()
        {
            Session["UserData"] = null;
            return RedirectToAction("Login", "Account");
        }
    }
}