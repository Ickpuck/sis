﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SIS.Lab1_2.Models;
using System.Security.Cryptography;

namespace SIS.Lab1_2.Controllers
{
    public class UsersController : Controller
    {
        private SisLab1DbContext db = new SisLab1DbContext();

        private Models.User AppUser
        {
            get
            {
                return Session["UserData"] as Models.User;
            }
        }

        // GET: Users
        public ActionResult Index()
        {
            if (AppUser == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (AppUser.IsInRole("Admin"))
            {
                return View(db.Users.ToList());
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (AppUser.Roles.First(r => r.Id == 1) != null && AppUser != null)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                User user = db.Users.Find(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                return View(user);
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            if (AppUser.Roles.First(r => r.Id == 1) != null && AppUser != null)
            {
                ViewBag.Roles = db.Roles.ToList();
                return View();
            }
            return RedirectToAction("Index", "Home");
        }

        // POST: Users/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Password,IdRole")] User user, List<int> roles)
        {
            if (roles.Count < 1)
            {
                ModelState.AddModelError("Name", "Pick a role");
            }
            if (ModelState.IsValid && roles.Count > 0)
            {
                var random = new RNGCryptoServiceProvider();
                byte[] salt = new byte[32];
                random.GetNonZeroBytes(salt);
                SaltedPassword pw = PasswordHasher.GenerateSaltedPassword(PasswordHasher.HashPassword(user.Password), salt);
                
                User newUser = new Models.User();
                newUser.Name = user.Name;
                newUser.Password = pw.PasswordHash;
                newUser.Salt = pw.Salt;

                newUser.Roles.Clear();
                foreach (Role role in db.Roles.Where(ro => roles.Contains(ro.Id)))
                {
                    newUser.Roles.Add(role);
                }
                db.Users.Add(newUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }            
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
