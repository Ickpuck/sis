﻿using SIS.Lab1_2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIS.Lab1_2.Controllers
{
    public class BLObjectsController : Controller
    {
        private SisLab1DbContext db;

        public BLObjectsController()
        {
            db = new SisLab1DbContext();
        }

        private User AppUser
        {
            get
            {
                return Session["UserData"] as Models.User;
            }
        }

        // GET: BLObjects
        public ActionResult Index()
        {
            return View(db.BLObjects.ToList());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(BLObject obj)
        {
            db.BLObjects.Add(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public ActionResult Edit(int id)
        {
            BLObject obj = db.BLObjects.FirstOrDefault(o => o.Id == id);

            return View(obj);
        }
        [HttpPost]
        public ActionResult Edit(BLObject obj)
        {
            db.BLObjects.Attach(obj);
            db.Entry(obj).State = System.Data.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            BLObject objToDelete = db.BLObjects.Find(id);
            return View(objToDelete);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BLObject obj = db.BLObjects.Find(id);
            db.BLObjects.Remove(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Write(int id)
        {
            BLObject obj = db.BLObjects.FirstOrDefault(o => o.Id == id);
            if (obj.SecurityLevel < AppUser.SecurityLevel && !AppUser.IsInRole("admin"))
            {
                return RedirectToAction("Index");
            }
            return View("Edit", obj);
        }
        [HttpPost]
        public ActionResult Write(BLObject obj)
        {
            db.BLObjects.Attach(obj);
            db.Entry(obj).State = System.Data.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Read(int id)
        {
            BLObject obj = db.BLObjects.FirstOrDefault(o => o.Id == id);
            if (obj.SecurityLevel > AppUser.SecurityLevel && !AppUser.IsInRole("admin"))
            {
                return RedirectToAction("Index");
            }
            return View(obj);
        }
    }
}