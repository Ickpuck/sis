namespace SIS.Lab1_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedsecuritylevelstamp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "WriteSecurityLevel", c => c.Int());
            AddColumn("dbo.Users", "ReadSecurityLevel", c => c.Int());            
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "WriteSecurityLevel");
            DropColumn("dbo.Users", "ReadSecurityLevel");
        }
    }
}
