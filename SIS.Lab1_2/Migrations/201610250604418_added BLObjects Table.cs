namespace SIS.Lab1_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedBLObjectsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BLObjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        WriteSecurityLevel = c.Int(nullable: false),
                        ReadSecurityLevel = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BLObjects");
        }
    }
}
