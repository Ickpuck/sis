namespace SIS.Lab1_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hotfix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "SecurityLevel", c => c.Int());
            AddColumn("dbo.BLObjects", "SecurityLevel", c => c.Int(nullable: false));
            DropColumn("dbo.Users", "WriteSecurityLevel");
            DropColumn("dbo.Users", "ReadSecurityLevel");
            DropColumn("dbo.BLObjects", "WriteSecurityLevel");
            DropColumn("dbo.BLObjects", "ReadSecurityLevel");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BLObjects", "ReadSecurityLevel", c => c.Int(nullable: false));
            AddColumn("dbo.BLObjects", "WriteSecurityLevel", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "ReadSecurityLevel", c => c.Int());
            AddColumn("dbo.Users", "WriteSecurityLevel", c => c.Int());
            DropColumn("dbo.BLObjects", "SecurityLevel");
            DropColumn("dbo.Users", "SecurityLevel");
        }
    }
}
