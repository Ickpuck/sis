﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SIS.Lab1_2.Models
{
    public class PasswordHasher
    {
        public static byte[] HashPassword(string password)
        {
            HashAlgorithm algorithm = new SHA256Managed();
            var data = Encoding.ASCII.GetBytes(password);
            return algorithm.ComputeHash(data);
        }
        
        public static SaltedPassword GenerateSaltedPassword(byte[] passwordHash, byte[] salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();
            SaltedPassword result = new SaltedPassword();

            byte[] plainTextWithSaltBytes =
              new byte[passwordHash.Length + salt.Length];
            for (int i = 0; i < passwordHash.Length; i++)
            {
                plainTextWithSaltBytes[i] = passwordHash[i];
            }
            for (int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[passwordHash.Length + i] = salt[i];
            }
            result.Salt = Convert.ToBase64String(salt);
            result.PasswordHash = Convert.ToBase64String(algorithm.ComputeHash(plainTextWithSaltBytes));

            return result;
        }

        public static bool VerifyPassword(string providedPassword, SaltedPassword existingPassword)
        {
            HashAlgorithm algorithm = new SHA256Managed();
            
            byte[] salt = Convert.FromBase64String(existingPassword.Salt);
            byte[] providedPasswordHash = HashPassword(providedPassword);

            SaltedPassword result = GenerateSaltedPassword(providedPasswordHash, salt);

            return result.PasswordHash == existingPassword.PasswordHash && result.Salt == existingPassword.Salt;
        }
    }
}