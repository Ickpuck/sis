﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIS.Lab1_2.Models
{
    public class SaltedPassword
    {
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
    }
}