﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIS.Lab1_2.Models
{
    public class BLObject
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public SecurityLevel SecurityLevel { get; set; }
    }

    public enum SecurityLevel
    {
        LowSecret = 0,
        Secret = 1,
        TopSecret = 2
    }
}