﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIS.Lab1_2.Models
{
    public class DiscreteObject
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ObjectId { get; set; }
        public string Name { get; set; }
        public bool Read { get; set; }
        public bool Write { get; set; }
        public bool Own { get; set; }
        public bool TG { get; set; }
    }
}