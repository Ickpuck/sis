﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIS.Lab1_2.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public SecurityLevel? SecurityLevel { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
        public User()
        {
            Roles = new List<Role>();
        }

        public bool IsInRole(string role)
        {
            return Roles.FirstOrDefault(r => r.Name.ToLower() == role.ToLower()) != null;
        }
    }
}