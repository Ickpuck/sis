﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIS.Lab1_2.Models
{
    public class LoginViewModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}