﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SIS.Lab1_2.Models
{
    public class SisLab1DbContext : DbContext
    {
        public SisLab1DbContext() : base("DefaultConnection") { }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<DiscreteObject> DiscreteObjects { get; set; }
        public DbSet<BLObject> BLObjects { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasMany(c => c.Roles)
                .WithMany(s => s.Users)
                .Map(t => t.MapLeftKey("RoleId")
                .MapRightKey("UserId")
                .ToTable("RoleUser"));
        }
    }
}